package pl.daprog.test.imagedownloader;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.daprog.test.imagedownloader.batch.BatchRepositoryConfiguration;
import pl.daprog.test.imagedownloader.batch.ImageDownloaderBatchConfiguration;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class SpringBootImageDownloaderApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootImageDownloaderApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        System.err.println("Zeby nie zapomniec o najwazniejszym - Hello World!");

        ApplicationContext context = new AnnotationConfigApplicationContext(BatchRepositoryConfiguration.class, ImageDownloaderBatchConfiguration.class);

        String filePath = args.length>0 ? args[0] : "links.txt";
        Map<String, JobParameter> params = new HashMap<>();
        params.put("filePath", new JobParameter(filePath));

        JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
        Job job = (Job) context.getBean("readImagesJob");

        JobExecution execution = jobLauncher.run(job, new JobParameters(params));
    }
}
