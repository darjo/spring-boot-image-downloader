package pl.daprog.test.imagedownloader.batch.internal;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

/**
 * Created by dariu_000 on 14.08.2016.
 */
public class SimpleFieldSetMapper implements FieldSetMapper<String> {

    @Override
    public String mapFieldSet(FieldSet fieldSet) throws BindException {
        return fieldSet.readString(0);
    }
}
