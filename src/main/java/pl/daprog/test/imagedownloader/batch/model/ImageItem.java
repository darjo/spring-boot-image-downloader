package pl.daprog.test.imagedownloader.batch.model;

import java.awt.image.BufferedImage;

/**
 * Created by dariu_000 on 14.08.2016.
 */
public class ImageItem {

    private String url;
    private BufferedImage image;

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
