package pl.daprog.test.imagedownloader.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import pl.daprog.test.imagedownloader.batch.internal.GetImageItemProcessor;
import pl.daprog.test.imagedownloader.batch.internal.ImageWriter;
import pl.daprog.test.imagedownloader.batch.model.ImageItem;

/**
 * Created by Darjo on 14.08.2016.
 */
@Configuration
@EnableBatchProcessing
public class ImageDownloaderBatchConfiguration {

    @Bean
    @StepScope
    public FlatFileItemReader<ImageItem> linksFileReader(@Value("#{jobParameters['filePath']}") String filePath) {
        FlatFileItemReader<ImageItem> reader = new FlatFileItemReader<>();
        reader.setResource(new FileSystemResource(filePath));
        reader.setLineMapper(new DefaultLineMapper<ImageItem>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] { "url" });
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper() {{
                setTargetType(ImageItem.class);
            }});
        }});
        return reader;
    }


    @Bean
    public GetImageItemProcessor getImageProcessor() {
        return new GetImageItemProcessor();
    }

    @Bean
    public ImageWriter imageWriter() {
        return new ImageWriter();
    }

    @Bean
    public TaskExecutor taskExecutor() {

        return new SimpleAsyncTaskExecutor();
    }

    @Bean
    public Step readImagesStep(StepBuilderFactory stepBuilderFactory, FlatFileItemReader<ImageItem> linksFileReader) {

        return stepBuilderFactory.get("readImagesStep")
                .<ImageItem, ImageItem> chunk(2)
                .reader(linksFileReader)
                .processor(getImageProcessor())
                .writer(imageWriter())
                .taskExecutor(taskExecutor())
                .build();
    }

    @Bean
    public Job readImagesJob(JobBuilderFactory jobs, Step readImagesStep) {
        return jobs.get("readImagesJob")
                .incrementer(new RunIdIncrementer())
                .flow(readImagesStep)
                .end()
                .build();
    }
}
