package pl.daprog.test.imagedownloader.batch.internal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemProcessor;
import pl.daprog.test.imagedownloader.batch.model.ImageItem;

import javax.imageio.ImageIO;
import java.net.URL;

/**
 * Created by dariu_000 on 14.08.2016.
 */
public class GetImageItemProcessor implements ItemProcessor<ImageItem, ImageItem> {

    private Log log = LogFactory.getLog(GetImageItemProcessor.class);

    @Override
    public ImageItem process(ImageItem item) throws Exception {

        log.debug("Read image in thread "+Thread.currentThread().getId());

        URL url = new URL(item.getUrl());
        item.setImage(ImageIO.read(url));
        return item;
    }
}
