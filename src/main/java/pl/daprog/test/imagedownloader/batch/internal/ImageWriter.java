package pl.daprog.test.imagedownloader.batch.internal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;
import pl.daprog.test.imagedownloader.batch.model.ImageItem;

import javax.imageio.ImageIO;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by dariu_000 on 14.08.2016.
 */
public class ImageWriter implements ItemWriter<ImageItem> {

    private Log log = LogFactory.getLog(ImageWriter.class);

    private static AtomicInteger counter = new AtomicInteger();

    private String getFileName(String url) {
        String fileName = url.substring( url.lastIndexOf('/')+1, url.length() );
        fileName = fileName.replace("?", "_").replace(":", "_");
        int lastIndex = fileName.lastIndexOf('.');
        if(lastIndex>=0) {
            return fileName.substring(0, lastIndex);
        } else {
            return fileName;
        }
    }

    @Override
    public void write(java.util.List<? extends ImageItem> items) throws Exception {

        log.debug("Write images in thread "+Thread.currentThread().getId());

        for(ImageItem item : items) {
            log.debug("Write image from "+item.getUrl());
            String destFileName = counter.getAndIncrement()+"_"+Thread.currentThread().getId()+"_"+getFileName(item.getUrl())+".jpg";
            ImageIO.write(item.getImage(), "jpg", new File(destFileName));
        }
    }
}
